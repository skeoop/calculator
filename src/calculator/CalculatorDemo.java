package calculator;

import java.util.Scanner;

import calculator.model.Calculator;

/**
 * demo use of Calculator class via command line.
 * The calculator is intended to have a GUI interface,
 * but invoking calculator directly promotes testing.
 * @author jim
 *
 */
public class CalculatorDemo {
	private static final String OPERATORS = "[-+*/^]";
	private static final String EQUALS = "=";
	private static Calculator calc = new Calculator();
	
	/** parse an input expression and give it to calculator.  Echo each argument to console. */
	public static void parseAndExecute(String expression) {
		Scanner scanner = new Scanner(expression);
		// input must consist of alternating numbers and operators.
		while( scanner.hasNext()) {
			if (scanner.hasNextDouble()) {
				double value = scanner.nextDouble();
				System.out.print(value); 
				System.out.print(' ');
				calc.setValue(value);
			}
			else if (scanner.hasNext(OPERATORS)) {
				char op = scanner.next(OPERATORS).charAt(0);
				System.out.print(op);
				System.out.print(' ');
				calc.setOperator(op);
			}
			else if (scanner.hasNext(EQUALS)) {
				scanner.next(EQUALS); // consume it
				System.out.print("= ");
				calc.perform();
				System.out.println(calc.getValue());
			}
			else {
				System.out.println("Unparsable value: " + scanner.next());
				return;
			}
		}
		scanner.close();
	}
	
	/** evaluate some test expressions */
	public static void testCalculator( ) {
		// this is a dumb parser. You must separate operators by space
		parseAndExecute("2 + 3 =");
		parseAndExecute("5 * 7 =");
		parseAndExecute("10 ^ 3 =");
		parseAndExecute("1 + 2 + 3 + 4 =");
		
		System.out.println("\nHow many seconds in a year?");
		parseAndExecute("365.25 * 24 * 60 * 60 =");
	}
	
	
	/** run calculator using console input */
	public static void console( ) {
		System.out.println("input an expression, ending with =.  Example: 2 * 4 + 3 * 11 =");
		System.out.println("The parser is lame. You must leave space around operators.");
		Scanner console = new Scanner(System.in);
		System.out.print("input: ");
		while( console.hasNextLine() ) {
			String line = console.nextLine().trim();
			if (! line.endsWith("=")) line += " =";
			parseAndExecute( line );
			System.out.print("input: ");
		}
	}
	
	public static void main(String[] args) {
		testCalculator( );
//		console( );	
	}
}
